#include "debug.h"
#include "values.h"
#ifdef RZERO
#include "wifi.h"
#include "webserver.h"
#endif
#include "display.h"
#include "sensors.h"

#ifdef SCAN_I2C
#include "scan_i2c.h"
#endif

void refreshSensors() {
  digitalWrite(LED_BUILTIN, LOW);
  read_values();
  format_values();

  display_values();

  digitalWrite(LED_BUILTIN, HIGH);
}

unsigned long startMillis;
unsigned long currentMillis;


void setup() {
  Serial.begin(115200);
  while ( !Serial ) delay(100); // wait for native usb

  setup_display();

  setup_sensors();

  #ifdef RZERO
  setup_wifi();
  setup_server();
  #endif

  #ifdef SCAN_I2C
  scan_i2c();
  #endif

  pinMode(LED_BUILTIN, OUTPUT);

  refreshSensors();
}

void loop() {
  currentMillis = millis();

  if (currentMillis - startMillis >= refreshInterval*1000) {
    refreshSensors();

    startMillis = currentMillis;
  }
}