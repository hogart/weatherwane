#pragma once

#ifdef SERIAL_DEBUG // wait for native usb
  #define debug(x) Serial.print(x)
  #define debugLn(x) Serial.println(x)
#else
  #define debug(x)
  #define debugLn(x)
#endif