#pragma once
#include <WString.h>

const String temp_scale = String(TEMP_SCALE);
const String temp_units = "°" + (temp_scale == "Re" ? "Ré" : temp_scale);

String format_temperature(float temperature) {
  float converted_temp = temperature;
  if (temp_scale == "F") {
    converted_temp = 1.8 * temperature + 32.0;
  } else if (temp_scale == "Re") {
    converted_temp = temperature * 0.8;
  } else if (temp_scale == "K") {
    converted_temp = temperature + 273.15;
  }

  return String(converted_temp, 1);
}