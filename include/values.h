#pragma once

#include "sensors.h"
#include "format_pressure.h"
#include "format_temperature.h"

constexpr unsigned long refreshInterval = 30;

#ifdef RZERO
float currentTemperature;
float currentPressure;
float currentAQI;
float currentHumidity;
float currentLux;
uint16_t currentRange;
String formattedTemperature = "";
String formattedPressure = "";
String formattedAQI = "";
String formattedHumidity = "";
String formattedLux = "";
bool enableDisplay = true;
uint8_t displayBrightness = 128;
#else
float currentRZero;
String formattedRZero = "";
#endif



void read_values() {
  #ifdef RZERO
  currentTemperature = bme.readTemperature();
  currentPressure = bme.readPressure() / 100.0f;
  currentHumidity = bme.readHumidity();
  currentAQI = mq135_sensor.getCorrectedPPM(currentTemperature, currentHumidity);
  #else
  currentRZero = mq135_sensor.getRZero();
  #endif

  if (bh1750.hasValue()) { // non blocking reading
    currentLux = bh1750.getLux();
    bh1750.start();
  }
}

void format_values() {
  #ifdef RZERO
  
  formattedTemperature = format_temperature(currentTemperature);

  formattedPressure = format_pressure(currentPressure);
  
  formattedAQI = String(currentAQI, 0);
  formattedHumidity = String(currentHumidity, 0);

  if (currentLux > 10000) {
    displayBrightness = 255;
  } else if (currentLux > 1000) {
    displayBrightness = 224;
  } else if (currentLux > 400) {
    displayBrightness = 192;
  } else if (currentLux > 200) {
    displayBrightness = 128;
  } else if (currentLux > 100) {
    displayBrightness = 96;
  } else {
    displayBrightness = 64;
  }

  #else
  formattedRZero = String(currentRZero, 0);
  #endif
}

void debug_values() {
  #ifdef RZERO
  debugLn("Temp: " + formattedTemperature);
  debugLn("Pressure: " + formattedPressure);
  debugLn("AQI: " + formattedAQI);
  debugLn("Hum: " + formattedHumidity);
  debugLn("Lux: " + String(currentLux) + ", Brightness: " + String(displayBrightness));
  #else
  debugLn(formattedRZero);
  #endif
}