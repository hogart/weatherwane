#pragma once

#include "SH1106Wire.h"
#include "display_fonts.h"

#include "values.h"
#include "debug.h"

const int displayAddress = 0x3c;

SH1106Wire display(displayAddress, SDA, SCL, GEOMETRY_128_64); 

#ifdef RZERO
const String splash = "Weather\n       wane v" + String(VERSION);
#else
const String splash = "Weather\n       wane v" + String(VERSION) + "\n       (calibration)";
#endif

String pad_units(String units) {
  uint8_t longest_unit = 3;

  if (pressure_units.length() > longest_unit) {
    longest_unit = pressure_units.length();
  }
  if (temp_units.length() > longest_unit) {
    longest_unit = temp_units.length();
  }

  String padded = String(units);

  if (padded.length() < longest_unit) {
    for (uint8_t i = 0; i < longest_unit - units.length(); i++) {
      padded = " " + padded;
    }
  }

  return padded;
}

String pressure_symbol(float hpa_pressure) {
  auto index = pressure_index(hpa_pressure);
  return display_font_pressure[index];
}

const String sep = ": ";

void setup_display() {
  debug("Connecting to display...");
  if (display.init()) {
    debugLn(" Connected!");
    
    if (SCREEN_FLIP) {
      display.flipScreenVertically();
    }
    
    display.setFont(Roboto_Mono_Light_12);
    display.drawString(0, 20, splash);
    display.display();
  } else {
    debugLn("Could not find a the display, check wiring or try a different address!");
  }
}

void draw_string(String str_to_display, uint8_t str_width) {
  display.clear();
  display.setBrightness(displayBrightness);
  display.drawString(
    random(0, 128 - str_width), 
    random(0, 7), 
    str_to_display
  );
}

void display_values() {
  #ifdef RZERO

  // temperature needs to be padded additionally, because degrees character is encoded in 2 bytes 
  const String temp_str = pad_units(" " + temp_units) + sep + formattedTemperature;
  const String pressure_str = pad_units(pressure_units) + sep + formattedPressure + pressure_symbol(currentPressure);
  const String hum_str = pad_units("RH") + sep + formattedHumidity;
  const String aqi_str = pad_units("AQI") + sep + formattedAQI;

  String str_to_display;
  str_to_display.concat(temp_str + "\n");
  str_to_display.concat(pressure_str + "\n");
  str_to_display.concat(hum_str + "\n");
  str_to_display.concat(aqi_str);

  // TODO: get a better way to determine the biggest string
  const uint8_t str_width = display.getStringWidth(pressure_str);

  draw_string(str_to_display, str_width);
  #else
  String str_to_display = "RZero: " + formattedRZero;

  draw_string(to_display, display.getStringWidth(to_display));
  #endif

  display.display();
}