#pragma once

#ifdef RZERO

#include <ESP8266WiFi.h>
#include "debug.h"

unsigned long startAttemptTime;
const unsigned long wifiTimeout = 10000; // Wi-Fi connection timeout in milliseconds

void setup_wifi() {
  if (!String(WIFI_SSID)) {
    debugLn("No wifi credentials provided, skipping connection");
    return;
  }

  debug("Connecting to wifi " + String(WIFI_SSID));
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  startAttemptTime = millis();
  while (WiFi.status() != WL_CONNECTED && millis() - startAttemptTime < wifiTimeout) {
    delay(500);
    debug(".");
  }

  if (WiFi.status() == WL_CONNECTED) {
    debugLn(" Connected!");
  } else {
    debugLn("Failed to connect: timeout.");
  }
}

#endif