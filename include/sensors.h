#pragma once

#include <Wire.h>
#include "Adafruit_BME280.h"
#include "MQ135.h"
#include <hp_BH1750.h>

#include "debug.h"
#include "values.h"

Adafruit_BME280 bme;

hp_BH1750 bh1750;

#ifdef RZERO
MQ135 mq135_sensor(A0, RZERO);
#else
MQ135 mq135_sensor(A0);
#endif

constexpr uint8_t bmpAddress = 0x76;

void setup_sensors() {
  Wire.begin();

  debug("Connecting to BME280...");
  if (!bme.begin(bmpAddress)) {
    debugLn("Could not find a BME280 sensor, check wiring or try a different address!");
  } else {
    debugLn(" Connected!");
  }

  debug("Connecting to BH1750...");
  bool avail = bh1750.begin(BH1750_TO_GROUND);
  if (!avail) {
    debugLn("Could not find a BH1750 sensor, check wiring!");
    bh1750.start();
  } else {
    debugLn(" Connected!");
  }
}