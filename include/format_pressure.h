#pragma once
#include <WString.h>

const String pressure_units = String(PRESSURE_SCALE);

/**
 * The font was modified to include custom symbols for high or low pressures, 
 * which looks like up and down chevrons. The new symbols have codes starting at 200 and ending at 205.
 * Single chevron up: the sky is clear or is going to clear soon
 * Double chevron up: unusually high pressure, may be a draught > 1022.689 hPa
 * Triple chevron up: find a shelter > 1040.000 hPa
 * Single chevron down: overcast and/or rainy
 * Double chevron down: unusually low pressure, serious precipitation < 1009.144 hPa
 * Triple chevron down: find a shelter: eye of a hurricane < 939
 * 
 * Characters: https://www.charset.org/utf-8
 */

const String display_font_pressure[] = {
  "È", // 200, triple chevron down, index = -3
  "É", // 201, double chevron down, index = -2
  "Ê", // 202, single chevron down, index = -1
  " ", // pressure normal, no chevron, index = 0
  "Ë", // 203, single chevron up, index = 1
  "Ì", // 204, double chevron up, index = 2
  "Í"  // 205, triple chevron up, index = 3
};

int8_t pressure_index(float hpa_pressure) {
    if (hpa_pressure > 1013.20) {
    return 4;
  } else if (hpa_pressure > 1022.689) {
    return 5;
  } else if (hpa_pressure > 1040.000) {
    return 6;
  } else if (hpa_pressure < 1012.40) {
    return 2;
  } else if (hpa_pressure < 1009.144) {
    return 1;
  } else if (hpa_pressure < 939) {
    return 0;
  } else {
    return 0;
  }
}

float convert_pressure(float hpa_pressure) {
  if (pressure_units == "mm Hg") {
    return 0.75006 * hpa_pressure;
  } else if (pressure_units == "in Hg") {
    return 0.02953 * hpa_pressure;
  } else {
    return hpa_pressure;
  }
}

String format_pressure(float hpa_pressure) {
  float converted = convert_pressure(hpa_pressure);

  return String(converted, 1);
}