#pragma once

#include <Wire.h>
#include "debug.h"

String num_to_hex(uint8_t num) {
  if (num < 16) {
    return "0" + String(num, HEX);
  } else {
    return String(num, HEX);
  }
}

void scan_i2c() {
  uint8_t error;
  uint8_t address;
  uint8_t nDevices;

  debugLn("Scanning...");

  nDevices = 0;
  for (address = 1; address < 127; address++) {
    // The i2c_scanner uses the return value of
    // the Write.endTransmission to see if
    // a device did acknowledge to the address.
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0) {
      debug("I2C device found at address 0x");
      debugLn(num_to_hex(address));

      nDevices++;
    } else if (error != 2) {
      debug("Error " + String(error) + " at address 0x");
      debugLn(num_to_hex(address));
    }
  }

  debugLn((nDevices == 0 ? "No" : String(nDevices)) + " device(s) found");
}