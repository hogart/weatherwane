#pragma once

#ifdef RZERO

#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

#include "values.h"

AsyncWebServer server(80);

const String pageTemplate = R"===(<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="refresh" content="{REFRESH_INTERVAL}" />
  <style>
    html {
      color-scheme: light dark;
      background-color: #f5f5f5;
      color: #333;
    }
    @media (prefers-color-scheme: dark) {
      html {
        background-color: #333;
        color: #f5f5f5;
      }
    }
    body {
      display: grid;
      place-items: center;
      min-height: 100vh;
      margin: 0;
    }
    main {  
      display: flex;
      flex-direction: column;
      align-items: center;
    }
    header {
      font-weight: 100;
      font-size: larger;
      opacity: 0.7;
      font-style: italic;
    }
    dt {
      opacity: 0.7;
    }
    dd {
      font-size: xx-large;
      font-weight: bolder;
      text-align: right;
      margin-bottom: 0.5rem;
    }
    .pressure_-3::after {
      content: '↓↓↓'    
    }
    .pressure_-2::after {
      content: '↓↓'
    }
    .pressure_-1::after {
      content: '↓'
    }
    .pressure_1::after {
      content: '↑'
    }
    .pressure_2::after {
      content: '↑↑'
    }
    .pressure_3::after {
      content: '↑↑↑'
    }
  </style>
  <title>Weatherwane</title>
</head>
<body>
  <main>
    <header>What a lovely weather!</header>
    <dl>
      <dt>Temperature:</dt>
      <dd>{TEMP} {TEMP_UNITS}</dd>
      <dt>Pressure:</dt>
      <dd class="pressure_{PRESSURE_INDEX}">{PRESSURE} {PRESSURE_UNITS}</dd>
      <dt>Humidity:</dt>
      <dd>{HUMIDITY} %</dd>
      <dt>Air Quality:</dt>
      <dd>{AQI}</dd>
    </dl>
  </main>
</body>
</html>
)===";

const String tempToken = "{TEMP}";
const String tempUnitsToken = "{TEMP_UNITS}";
const String pressureToken = "{PRESSURE}";
const String pressureUnitsToken = "{PRESSURE_UNITS}";
const String pressureIndexToken = "{PRESSURE_INDEX}";
const String aqiToken = "{AQI}";
const String humidityToken = "{HUMIDITY}";
const String refreshToken = "{REFRESH_INTERVAL}";

String renderHtml() {
  String page = pageTemplate;
  page.replace(tempToken, formattedTemperature);
  page.replace(tempUnitsToken, temp_units);
  page.replace(pressureToken, formattedPressure);
  page.replace(pressureUnitsToken, pressure_units);
  page.replace(aqiToken, formattedAQI);
  page.replace(humidityToken, formattedHumidity);
  page.replace(pressureIndexToken, String(pressure_index(currentPressure)));
  page.replace(refreshToken, String(refreshInterval));

  return page;
}

void handle_OnConnect(AsyncWebServerRequest *request) {
  request->send(200, "text/html", renderHtml()); 
}

void handle_NotFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

void setup_server() {
  if (WiFi.status() != WL_CONNECTED) {
    debugLn("No Wi-Fi connection, skipping server setup");
    return;
  }
  
  debug("Starting webserver...");
  server.on("/", HTTP_GET, handle_OnConnect);
  server.onNotFound(handle_NotFound);
  
  server.begin();
  debugLn(" Started at http://" + WiFi.localIP().toString() + "/");
}

#endif