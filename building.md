## 1. Buy components
  Don't by cheap jumper wires and breadboards. They are more trouble than they worth.
   
## 2. If you don't have any, buy a soldering iron. 
Any 60W with a small tip will do, but if you want to splurge, buy Pinecil — it's very handy, smart, open-source and just fun. Also buy:
* some rosin-core solder
* desoldering wick
* a soldering iron stand, so as not to damage your desk and yourself
* some isopropyl alcohol, the rosin is slightly acidic (the point of a flux is to remove the oxide layer), so you need to wash the contact points after the soldering.
  
## 3. Install VS Code and the Platform.io extension.
The code won't work with the Arduino IDE.

## 4. Solder the connector pins
Use spare pins to align the component parallel to the breadboard

## 5. Connect everything
Be very mindful regarding `ground` (`-`) and `+` lines. MQ135 requires +5v, so it connects on the other side of the NodeMCU board.

## 6. Connect the NodeMCU to your computer
Make sure it is listed in the devices list in Platform.io. Use a decent USB cable and avoid USB hubs.

## 7. Click "upload and monitor"
Check if everything works

## 8. Calibrate the MQ135
According to the library documentation, the sensor needs to run for 12-24 hours at temperatures between 15 and 25 Celsius, somewhere with access to the outside, fresh air, so a balcony should do. 
* Use a powerbank if necessary, 10.000 mAh should suffice.
* Avoid direct sunlight.
* To launch the device in calibration mode, find and delete or comment this line in platformio.ini: `'-D RZERO=11.0'`.
* After at least 12 hours, note the RZERO value the display shows and put it into the platformio.ini. Uncomment the line.

## 9. Customize the device

There's a bunch of variables you can tweak in platformio.ini to customize the device to your needs and preferences. Do you prefer your temperature in Fahrenheit? No problem. Kelvin? It's a safe space. More used to pressure being in millimeters of mercury? Got you covered.
* Make sure to put in your Wi-fi SSID and password.
* Depending on how you assembled the device, you may need to rotate the screen.