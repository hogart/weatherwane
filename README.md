# Weatherwane

Weatherwane is a very basic ESP8266 weather station.

## Features

* Temperature, relative humidity, atmospheric pressure
* AQI
* Sleek web interface
* OLED display with adaptive brightness and burn-out protection
* MQ135 calibration mode
* Supports Fahrenheit/Celsius/Kelvin/Réaumur temperature scales
* Supports hPa/mm Hg/in Hg pressure units
* Indication of increased and decreased pressure

## Components

* NodeMCU v3 microcontroller
* SH1106 OLED 128x64 display
* BME/BMP280 temperature, humidity and pressure sensor
* BH1750 light intensity sensor
* MQ135 air quality sensor
* wires, breadboard, USB cable

## Roadmap

* Display info during startup
* Indicate air quality condition using an RGB LED
* Use a presence sensor to turn display on/off when not in use
* Settings web-page
* Make sensor reading, display refresh and website refresh independent intervals
  * investigate why the current display library doesn't work with the Ticker lib